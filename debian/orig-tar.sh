#!/bin/sh -e

VERSION=$2
DOWNLOADED_FILE=$3
PACKAGE=$(dpkg-parsechangelog | sed -n 's/^Source: //p')
TAR=../${PACKAGE}_${VERSION}+dfsg1.orig.tar.gz
DIR=${PACKAGE}-${VERSION}

svn export svn://svn.freehep.org/svn/freehep/tags/vectorgraphics-${VERSION}/$PACKAGE $DIR
rm -f $DIR/src/examples/image/image.ai
rm -f $DIR/src/examples/basic/*.swf
rm -f $DIR/src/examples/image/image-jpeg.swf
rm -f $DIR/src/test/resources/swf/*.swf
rm -f $DIR/src/examples/extended/*.swf
GZIP=--best tar -c -z -f $TAR --exclude '*.jar' --exclude '*.class' $DIR
rm -rf $DIR

rm -f $DOWNLOADED_FILE

